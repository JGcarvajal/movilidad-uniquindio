package com.movilidaduniquindio;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

/**
 * Esta es la clase que nos permite la toma de coordenadas cuando se realiza un registro o se va a
 * registrar una direccion
 */
public class Coordenadas extends AppCompatActivity implements GoogleMap.OnMarkerDragListener,OnMapReadyCallback {

    private GoogleMap mMap;
    private Marker miMarker;
    private double latitud;
    private double longitud;
private Button btnAceptar;
Preference preference;

    /**
     * Este es el metodo principal de la actividad, encargado de crear la clase
     * @param savedInstanceState parametro que guarda la instancia de la clase
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordenadas);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnAceptar=(Button)findViewById(R.id.btnAceptar);

        btnAceptar.setOnClickListener(btnAceptarListener);


    }
 private Button.OnClickListener btnAceptarListener =new View.OnClickListener() {
     @Override
     public void onClick(View v) {
         guardarCoordenadas(latitud+";"+longitud);

         Coordenadas.this.finish();
         /**  Intent intent =new Intent(Coordenadas.this,Registro.class);
          Coordenadas.this.startActivity(intent);**/
     }
 };

    /**
     * Metodo encargado de instanciar el mapa
     * @param googleMap parametro que instancia el mapa
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
       try{
           if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                   && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

           } else {
                   mMap = googleMap;
                miUbicacion();
                googleMap.setOnMarkerDragListener(this);

                mMap.setOnMapClickListener(clickListener);
                mMap.setMyLocationEnabled(true);
            }
       }
       catch (Exception ex){

       }
    }

    /**
     * Metodo encargado de escuchar el evento de chick o toque en el mapa
     */
    private GoogleMap.OnMapClickListener clickListener =
            new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                   latitud=latLng.latitude;
                    longitud =latLng.longitude;

                    agregarMarker(latitud,longitud);

                
                }
            };

    /**
     * Metodo encargado de posicionar un marcador en el mapa
     * @param latitud del marcador a posicionar
     * @param longitud del marcador a posicionar
     */
    public void agregarMarker(double latitud, double longitud) {
       if (miMarker !=null) {
           miMarker.remove();
       }
        LatLng ubicacion = new LatLng(latitud, longitud);
        miMarker = mMap.addMarker(new MarkerOptions().position(ubicacion).title("Mi Ubicación")
                .snippet("Arrastra para tomar coordenadas").draggable(true)
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(ubicacion, 16);
        mMap.animateCamera(miUbicacion);
    }

    /**
     * Metodo encargado de actualizar la ubicacion de un marcador
     * @param location ubicacion del marcador a actualizar
     */
    private void actualizarubicacion(Location location) {
        if (location != null) {
            latitud = location.getLatitude();
            longitud = location.getLongitude();
            agregarMarker(latitud, longitud);
        }
    }

    /**
     * Metodo encargado de obtener la ubicacion del usuario
     */
    private void miUbicacion() {
        try {
            double lat;
            double longi;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            } else {


                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


                if (locationManager != null) {
                    //Existe GPS_PROVIDER obtiene ubicación
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }

                if (location == null) { //Trata con NETWORK_PROVIDER

                    if (locationManager != null) {
                        //Existe NETWORK_PROVIDER obtiene ubicación
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                if (location != null) {
                    actualizarubicacion(location);
                    lat = location.getLatitude();
                    longi = location.getLongitude();
                    guardarCoordenadas(lat + ";" + longi);
                } else {
                    Toast.makeText(this, "No se pudo obtener geolocalización", Toast.LENGTH_LONG).show();
                }


            }
        }catch (Exception ex){

        }
    }

    /**
     * Metodo encargado de escuchar el evento de cuando se inicia a arrastrar un marcador
     * @param marker marcador que se arrastra
     */
    @Override
    public void onMarkerDragStart(Marker marker) {

    }


    /**
     * Metodo encargado de escuchar el evento de cuando se arrastra un marcador
     * @param marker marcador que se arrastra
     */
    @Override
    public void onMarkerDrag(Marker marker) {
        double lat=marker.getPosition().latitude;
        double log=marker.getPosition().longitude;
        String newTitle=String.format(Locale.getDefault(),
                lat+" "+log,lat,log);

        setTitle(newTitle);
    }


    /**
     * Metodo encargado de escuchar el evento de cuando se finaliza de arrastrar un marcador
     * @param marker marcador que se arrastra
     */
    @Override
    public void onMarkerDragEnd(Marker marker) {
        setTitle(R.string.coordenadas);

    }

    /**
     * este metodo se encarga de guardar las coordenadas en un archivo de preferencias para ser
     * recuperadas mas adelante
     * @param coordenadas a guardar
     */
    private void guardarCoordenadas(String coordenadas) {

        SharedPreferences settings = getSharedPreferences(Constantes.ALMACENAR_COORDENADAS, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constantes.COORDENADAS, coordenadas);
        editor.commit();
    }

}
