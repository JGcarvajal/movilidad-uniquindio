package com.movilidaduniquindio;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Esta es la clase adaptador para mapear los servicios en la clase cardView
 */
public class ReciclerVewAdaptador extends RecyclerView.Adapter<ReciclerVewAdaptador.ViewHolder> {
    private OnItemClickListener mListener;
    public interface OnItemClickListener{
    void onItemClick(int position);
    }

    public void setOnItemClickListener (OnItemClickListener listener){
        mListener=listener;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        private String numDoc;
        private String idVehiculo;
        private LatLng latLngInicio;
        private LatLng latLngFin;
        private TextView fecha;
        private TextView hora;
        private TextView conductor;
        private TextView puestos;
        private TextView observacion;

        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            fecha=(TextView)itemView.findViewById(R.id.tvFecha);
            hora=(TextView)itemView.findViewById(R.id.tvHora);
            conductor=(TextView)itemView.findViewById(R.id.tvConductor);
            conductor.setTypeface(null,Typeface.BOLD);
            observacion=(TextView)itemView.findViewById(R.id.tvDescripcion);
            puestos=(TextView)itemView.findViewById(R.id.tvPuestos);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(listener!=null){
                    int position=getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
                }
            });
        }
    }

    private List<Servicio> servicioList;

    /**
     * Metodo que instancia la lista de servicios a mapear
     * @param servicioList lista de servicios a mapear
     */
    public ReciclerVewAdaptador(List<Servicio> servicioList) {
        this.servicioList = servicioList;
    }

    /**
     * Metodo principal para mapear los objetos
     * @param viewGroup
     * @param i
     * @return objeto para mapear los servicios
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_servicio,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view,mListener);
        return viewHolder;
    }

    /**
     * Metodo encargado de mapear los servicios en el CardView
     * @param viewHolder Objeto para mapear
     * @param i posicion del objeto en el array
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.conductor.setText(servicioList.get(i).getConductor());
        viewHolder.hora.setText(servicioList.get(i).getHora());
        viewHolder.fecha.setText(servicioList.get(i).getFecha());
        viewHolder.observacion.setText(servicioList.get(i).getObservacion());
        viewHolder.puestos.setText(Integer.toString(servicioList.get(i).getPuestos()));
    }

    /**
     * Metodo para obtener el tamaño del array de servicios
     * @return regresa el tamaño del array
     */
    @Override
    public int getItemCount() {
        return servicioList.size();
    }
}
