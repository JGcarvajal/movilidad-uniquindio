package com.movilidaduniquindio;

/**
 * Es es la clase de constantes se usaran en todo el proyecto
 */
public class Constantes {
    public static final String ALMACENAR_COORDENADAS= "almacenar_coordenadas";
    public static final String COORDENADAS= "coordenadas";
    public static final String SERVIDOR= "http://movilidaduq.000webhostapp.com";

    public final static int RESP_PERMISOS_LOCATION = 2005;
    public final static int RESP_PERMISOS_STORAGE = 2003;
    public final static int RESP_PERMISOS_PHONE = 2004;
    public final static int RESP_PERMISOS_CAMERA = 2006;
    public final static int REP_CARGAR_IMAGEN = 1001;
    public final static int REP_TOMAR_FOTO = 1002;
    public static final int ENABLE_GPS = 1100;
    public static final String CARPETA_RAIZ = "movilidadUniquindio/";
    public static final String RUTA_IMAGEN = "misFotos";

}
