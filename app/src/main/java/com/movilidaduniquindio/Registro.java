package com.movilidaduniquindio;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Clase que registra un usuario en la aplicacion
 */
public class Registro extends AppCompatActivity  {
    private Spinner spFacultades;
    private TextView fechaNacimiento;
    private Usuario usuario;
    private Button btnregistrar;
    private ImageButton btnCoordenadas;
    private TextView idCoordenadas;
    private EditText eTnombre;
    private EditText eTapellidos;
    private EditText eTtelefono;
    private EditText eTcorreo1;
    private EditText eTcorreo2;
    private EditText eTclave1;
    private EditText eTclave2;
    private EditText eTidentificacion;
    private EditText eTdireccion;
    RegisterRequest registerRequest;
    private ImageView foto;
    private Bitmap bitmap;
    public static final String pathFoto="prueba";
    DatePickerDialog.OnDateSetListener myFechaListener;

    /**
     * Este es el metodo principal de la actividad, encargado de registrar al usuario
     * @param savedInstanceState parametro que guarda la instancia de la clase
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spFacultades=(Spinner)findViewById(R.id.spFacultades);
        fechaNacimiento=(TextView)findViewById(R.id.fechaNacimiento);

        eTnombre=(EditText)findViewById(R.id.etNombre);
        eTapellidos=(EditText)findViewById(R.id.etApellidos);
        eTtelefono=(EditText)findViewById(R.id.etCelular);
        eTcorreo1=(EditText)findViewById(R.id.etCorreo1);
        eTcorreo2=(EditText)findViewById(R.id.etCorreo2);
        eTclave1=(EditText)findViewById(R.id.etclave1);
        eTclave2=(EditText)findViewById(R.id.etclave2);
        eTidentificacion=(EditText)findViewById(R.id.etIdentificacion);
        eTdireccion=(EditText)findViewById(R.id.etDireccion);
        btnregistrar=(Button)findViewById(R.id.btnRegistrar);
        btnCoordenadas=(ImageButton)findViewById(R.id.ibtCoordenadas);
        idCoordenadas=(TextView)findViewById(R.id.idCoordenadas);
        pintaButton(obtenerCoordenadas());
        foto=(ImageView) findViewById(R.id.imgFoto);

        btnregistrar.setOnClickListener(btnregistrarListener);
        fechaNacimiento.setOnClickListener(FNacimientoListener);
        spFacultades.setOnItemSelectedListener(spFacultadesListener);
        btnCoordenadas.setOnClickListener(coordenadasListener);

        eTcorreo2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String correo1=eTcorreo1.getText().toString();
                if(!correo1.equals(s.toString()))
                    eTcorreo2.setError("Los correos no coinciden");
            }
        });

        eTclave2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String clave1=eTclave1.getText().toString();
                if(!clave1.equals(s.toString()))
                    eTclave2.setError("Las claves no coinciden");

            }
        });

    }
    /**
     * Metodo encargado de escuchar el evento del texto registrar
     */
    private TextView.OnClickListener btnregistrarListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        if(eTcorreo1.getText().toString().equals(eTcorreo2.getText().toString()) ) {
            if(eTclave1.getText().toString().equals(eTclave2.getText().toString())) {

                final String nombres = eTnombre.getText().toString().trim();
                final String apellidos = eTapellidos.getText().toString().trim();
                final String identificacion = eTidentificacion.getText().toString().trim();
                final String telefono = eTtelefono.getText().toString().trim();
                final String correo = eTcorreo1.getText().toString().trim();
                final String clave = eTclave1.getText().toString().trim();
                final String direccion = eTdireccion.getText().toString().trim();
                final String facultad = spFacultades.getSelectedItem().toString().trim();
                final String fechaNac = fechaNacimiento.getText().toString().trim();
                final String coordenadas[] = obtenerCoordenadas().split(";");
                final String lat = coordenadas[0].trim();
                final String log = coordenadas[1].trim();
                final String imagen = imageToString(bitmap);
                final String extension = ".jpg";


                usuario = new Usuario(nombres, apellidos, telefono, correo, clave, identificacion,
                        facultad, fechaNac, direccion, lat, log,imagen,extension);
                if (validarDatos(usuario)) {
                    Response.Listener<String> responsListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("PRUEBAAAAAA", response);
                                JSONObject jsonResponse = new JSONObject(response);

                                boolean success = jsonResponse.getBoolean("success");

                                if (success) {
                                    Toast.makeText(getBaseContext(), "Registro exitoso ", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Registro.this, Servicios.class);
                                    Registro.this.startActivity(intent);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Registro.this);
                                    builder.setMessage("Error en el registro").setNegativeButton("OK", null)
                                            .create().show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    RegisterRequest registerRequest = new RegisterRequest(usuario, responsListener);
                    RequestQueue requestQueue = Volley.newRequestQueue(Registro.this);
                    requestQueue.add(registerRequest);
                }
            }else{
                Toast.makeText(Registro.this, "Las claves no coinciden ", Toast.LENGTH_LONG).show();
            }
        } else{
            Toast.makeText(Registro.this, "Los correos no coinciden ", Toast.LENGTH_LONG).show();
        }


    }
};
    /**
     * Metodo encargado de escuchar elevento click del texto Fecha Nacimiento
     */
    private TextView.OnClickListener FNacimientoListener =new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar calendar = Calendar.getInstance();
            int year=calendar.get(Calendar.YEAR);
            int month=calendar.get(Calendar.MONTH);
            int day=calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog=new DatePickerDialog(Registro.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            fechaNacimiento.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                        }
                    },year,month,day );


            datePickerDialog.show();
        }
    };

    /**
     * Metodo encargado de eschuchar elevento el Spinner facultades
     */
    private Spinner.OnItemSelectedListener spFacultadesListener=new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //Toast.makeText(parent.getContext(),"Seleccionó: " +parent.getItemAtPosition(position).toString(),Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
};
    /**
     * metodo escuchador encargado de escuchar los eventos del boton coordenadas
     */
    private Button.OnClickListener coordenadasListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(Registro.this,Coordenadas.class);
            Registro.this.startActivity(intent);
        }
    };
    /**
     * Metodo encargado de obtener las coordenadas almacenadas en el archivo de preferencias
     * @return retorna las coordenadas recuperadas
     */
    private String obtenerCoordenadas() {

        SharedPreferences settings = getSharedPreferences(Constantes.ALMACENAR_COORDENADAS, MODE_PRIVATE);
        return settings.getString(Constantes.COORDENADAS, "");
    }

    /**
     * Metodo encargado informar si se han tomado las corrdenadas exitosamento
     * @param coodenadas coordenadas capturadas
     */
    private void pintaButton(String coodenadas){

        if(!coodenadas.equals("")){
           idCoordenadas.setText("Sin capturar");
        }else{
            idCoordenadas.setText("Capturadas");

        }
    }
    /**
     * Este es el metodo encargado de validar los campos de la pantalla crear usuario, el metodo
     * valida que los campos no esten vacios y que el correo sea universitario
     * @param usuario usuario a validar
     * @return regresa true si el usuario es valido caso contrario false
     */
    public boolean validarDatos(Usuario usuario) {
        try {
            if (usuario == null || usuario.getNombres().equals("") || usuario.getApellidos().equals("") ||
                    usuario.getClave().equals("") || usuario.getDireccion().equals("") || usuario.getCorreo().equals("") ||
                    usuario.getFacultad().equals("") || usuario.getfNacimiento().equals("") || usuario.getIdentificacion().equals("") ||
                    usuario.getLatitud().equals("") || usuario.getLongitud().equals("") || usuario.getTelefono().equals("")) {
                Toast.makeText(Registro.this, "Por favor llene todos los campos ", Toast.LENGTH_LONG).show();
                return false;
            } else {
                final String[] correo = usuario.getCorreo().split("@");

                if (correo.length == 2) {
                    if (correo[1].equals("uqvirtual.edu.co") || correo[1].equals("uniquindio.edu.co")) {
                        Pattern pattern=Pattern.compile("[a-z A-Z]+");
                        if(pattern.matcher(usuario.getNombres()).matches()==false){
                            Toast.makeText(Registro.this, "El nombre contiene caracteres no permitidos ", Toast.LENGTH_LONG).show();
                            return false;
                        }
                        return true;
                    } else {
                        eTcorreo1.setError("Este no es un correo valido");
                        return false;
                    }

                }



            }

        }
        catch(Exception ex ){

        }  return false;
    }

    /**
     * Metodo encargado de escuchar el evento click de la imagen
     * @param view
     */
    public void CargarImagen(View view) {
        caragarFoto();
    }

    /**
     * Este metodo se encaga de llamar el dialog para seleccionar la aplicacion para cargar la imagen
     */
    private void caragarFoto() {

        final CharSequence[] opciones ={"Tomar Foto","Cargar Imagen","Cancelar"};
        final AlertDialog.Builder alertOpciones=new AlertDialog.Builder(Registro.this);
        alertOpciones.setTitle("Seleccione una opción");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(opciones[which].equals("Tomar Foto")) {
                    tomarfotografia();
                }else{
                if(opciones[which].equals("Cargar Imagen")){
                    Intent intent=new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/");
                    startActivityForResult(intent.createChooser(intent,"Selecione una aplicacion"),Constantes.REP_CARGAR_IMAGEN);
                }else{
                    dialog.dismiss();
                }
            }
            }
        });

        alertOpciones.show();
    }

    private void tomarfotografia() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String nombre=DirApp().getPath()+"-imagenUsuario.jpg";


        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),BuildConfig.APPLICATION_ID+".provider",
                        new File(nombre));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constantes.REP_TOMAR_FOTO);


    }


    /**
     * Este metodo se encarga de pasar la foto al elemento imageView
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
        if(resultCode==RESULT_OK){
            switch (requestCode){
                case Constantes.REP_CARGAR_IMAGEN:
                    Uri miPath=data.getData();
                    bitmap=MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),
                            miPath);
                    foto.setImageBitmap(bitmap);
                    break;

                case Constantes.REP_TOMAR_FOTO:
                    Drawable imagen=resizedImageAdic1(400,400);
                    if (imagen!=null){
                         bitmap = ((BitmapDrawable) imagen).getBitmap();
                        foto.setImageBitmap(bitmap);
                    }



                    break;
            }
        }} catch (Exception e ){

        }


    }

    public Drawable resizedImageAdic1(int newWidth, int newHeight) {

        Matrix matrix;
        FileInputStream fd = null;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;

        try {
            File fileImg = new File(DirApp(), "imagenUsuario.jpg");

            if (fileImg.exists()) {

                fd = new FileInputStream(fileImg.getPath());
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inDither = false;
                option.inPurgeable = true;
                option.inInputShareable = true;
                option.inTempStorage = new byte[32 * 1024];
                option.inPreferredConfig = Bitmap.Config.RGB_565;

                bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD(), null, option);

                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();

                // Reescala el Ancho y el Alto de la Imagen
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                // Crea la Imagen con el nuevo Tamano
                resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
                BitmapDrawable bmd = new BitmapDrawable(resizedBitmap);

                bitmapOriginal.recycle();
                bitmapOriginal = null;
                return bmd.mutate();
            }

            return null;

        } catch (Exception e) {

            return null;

        } finally {
            if (fd != null) {

                try {

                    fd.close();

                } catch (IOException e) {
                }
            }
            matrix = null;
            resizedBitmap = null;
            bitmapOriginal = null;
        }
    }

    public static File DirApp() {

        File SDCardRoot = Environment.getExternalStorageDirectory(); //Environment.getDataDirectory(); //
        File dirApp = new File(SDCardRoot.getPath() + "/" +pathFoto );

        //File dirApp = new File(SDCardRoot + "/data/" + "co.com.SuperRicas/" + Const.nameDirApp);

        if (!dirApp.isDirectory())
            dirApp.mkdirs();

        return dirApp;
    }

    public String imageToString(Bitmap bitmap){
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,array);

        byte[] imageByte=array.toByteArray();
        String imageString= Base64.encodeToString(imageByte,Base64.DEFAULT);

        return imageString;
    }
}


