package com.movilidaduniquindio;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Esta es la clase de conexion para realizar la peticion de logueo en la aplicacion
 */
public class LoguinRequest extends StringRequest {

    private static final String LOGUIN_REQUEST_URL=Constantes.SERVIDOR+"/servicios/LoginPHP.php";
    private Map<String,String> params;

    /**
     * metodo encargado de hacer la peticion http al servidor web
     * @param listener este es parametro escuchador de la respuesta del servidor
     */
    public LoguinRequest (String correo, String clave, Response.Listener<String> listener){
        super(Request.Method.POST,LOGUIN_REQUEST_URL,listener,null);
        params=new HashMap<>();
        params.put("correo",correo);
        params.put("clave",clave);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}