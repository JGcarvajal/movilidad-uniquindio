package com.movilidaduniquindio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Esta es la clase 2 de 2 de las clases encargadas de crear un servicio
 */
public class CrearServicio2 extends AppCompatActivity {

    private TextView tvFecha,tvHora;
    private EditText etObservacion,etPuestos;
    private AlertDialog.Builder alertDialog;
    Date dFecha,dHora;
    private Usuario usuario;
    Coordenadas coordenads;
    Preference preference;
    Servicio servicio;
    CrearServiciosRequest crearServiciosRequest;
    private TextView tvCrearServicio;

    /**
     * Este es el metodo principal de la actividad, encargado de crear la clase
     * @param savedInstanceState parametro que guarda la instancia de la clase
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_servicio2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvFecha=(TextView)findViewById(R.id.tvFecha);
        tvHora=(TextView)findViewById(R.id.tvhora);
        etObservacion=(EditText) findViewById(R.id.etObservacion);
        etPuestos=(EditText)findViewById(R.id.etPuestos);
        usuario = Preference.getSavedObjectFromPreference(this,
                "mPreference", "USER", Usuario.class);
        servicio= Preference.getSavedObjectFromPreference(this,
                "mPreference", "SERVICIO", Servicio.class);
        tvCrearServicio=(TextView)findViewById(R.id.tvCrear);
        tvCrearServicio.setOnClickListener(tvCrearServicioListener);

        alertDialog=new AlertDialog.Builder(this);

        tvFecha.setOnClickListener(tvFechaListener);

        tvHora.setOnClickListener(tvHoraListener);
    }
    private TextView.OnClickListener tvHoraListener =new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dHora=new Date();
            final int hour=dHora.getHours();
            int minute=dHora.getMinutes();

            TimePickerDialog timePickerDialog=new TimePickerDialog(CrearServicio2.this,
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            tvHora.setText(hourOfDay+":"+minute);

                            dHora.setHours(hour);
                            dHora.setMinutes(minute);
                        }
                    },hour,minute,false);


            timePickerDialog.show();
        }
    };

    private TextView.OnClickListener tvFechaListener =new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            dFecha=new Date();
            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar ca=Calendar.getInstance();
            String date[] =dateFormat.format(ca.getTime()).split("/");

            int year=Integer.parseInt(date[0]);
            int month=Integer.parseInt(date[1])-1;
            final int day=Integer.parseInt(date[2]);

            final DatePickerDialog datePickerDialog=new DatePickerDialog(CrearServicio2.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            tvFecha.setText(year+"/"+(month+1)+"/"+dayOfMonth);

                            try {
                                dFecha=dateFormat.parse(year+"/"+month+"/"+dayOfMonth);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    },year,month,day);


            datePickerDialog.show();
        }
    };
    /**
     * Metodo encargado de escuchar el evento del click sobre el boton Crear Servicio
     */
    private View.OnClickListener tvCrearServicioListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
            SimpleDateFormat timeFormat = new SimpleDateFormat("hhmm", Locale.getDefault());
            SimpleDateFormat horaFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());

            final String numDoc = usuario.getIdentificacion()+tvFecha.getText().toString().replace("/","")+
                    tvHora.getText().toString().replace(":","");

            final String fecha=tvFecha.getText().toString();
            final String hora=tvHora.getText().toString();
            final String conductor=usuario.getIdentificacion();
            final int puestos=Integer.parseInt(etPuestos.getText().toString());
            final String observacion=etObservacion.getText().toString();

            servicio.setNumDoc(numDoc);
            servicio.setFecha(fecha);
            servicio.setHora(hora);
            servicio.setConductor(conductor);
            servicio.setPuestos(puestos);
            servicio.setObservacion(observacion);
            servicio.setIdVehiculo("MZC78B");

            if(verificarServicio(servicio)) {

                Response.Listener<String> responsListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            //Toast.makeText(getBaseContext(), "Creo el servicio?!! "+success, Toast.LENGTH_LONG).show();
                            if (success) {
                                Toast.makeText(getBaseContext(), "Servicio creado con exito!! ", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(CrearServicio2.this, Servicios.class);
                                CrearServicio2.this.startActivity(intent);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(CrearServicio2.this);
                                builder.setMessage("Error en el registro").setNegativeButton("OK", null)
                                        .create().show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
                CrearServiciosRequest crearServiciosRequest = new CrearServiciosRequest(servicio, responsListener);
                RequestQueue requestQueue = Volley.newRequestQueue(CrearServicio2.this);
                requestQueue.add(crearServiciosRequest);
            }else{
                Toast.makeText(getBaseContext(),"Por favor llene todos los campos ",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            alertDialog.setTitle("Atención");
            alertDialog.setMessage("Ha ocurrido un error, verifique que se llenaron todos los campos");
            alertDialog.show();
        }
        }
    };

    /**
     * Este es el metodo encargado de validar los campos de la pantalla crear servicio, el metodo
     * valida que los campos no esten vacios
     * @param servicio servicio a validar
     * @return regresa true si el servicio es valido caso contrario false
     */
    private boolean verificarServicio(Servicio servicio){

        if(servicio==null || servicio.getNumDoc()==null|| servicio.getLatLngInicio()==null||
                servicio.getLatLngFin()==null||servicio.getFecha()==null||servicio.getHora()==null||
                servicio.getIdVehiculo()==null){
            return false;
        }
        else{

            return true;
        }
    }
}
