package com.movilidaduniquindio;

/**
 * clase del objeto usuario
 */
public class Usuario {
    private String nombres;
    private String apellidos;
    private String telefono;
    private String correo;
    private String clave;
    private String identificacion;
    private String facultad;
    private String fNacimiento;
    private String direccion;
    private String latitud;
    private String longitud;
    private String imagen;
    private String extension;

    /**
     * Constructor vacio de la clase
     */
    public Usuario() {
    }

    /**
     * Contructor de la clase
     * @param nombres nombres del usuario
     * @param apellidos apellidos del usuario
     * @param telefono telefono del usuario
     * @param correo correo del usuario
     * @param clave clave del usuario
     * @param identificacion identificacion del usuario
     * @param facultad faculta a la que esta abscrito el usuario
     * @param fNacimiento fecha de nacimiento del usuario
     * @param direccion direccion de recidencia del usuario
     * @param latitud latitud de la recidencia del usuario
     * @param longitud longitud de la recidencia del usuario
     */
    public Usuario(String nombres, String apellidos, String telefono, String correo, String clave,
                   String identificacion, String facultad, String fNacimiento, String direccion,
                   String latitud, String longitud,String imagen, String extension) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.correo = correo;
        this.clave = clave;
        this.identificacion = identificacion;
        this.facultad = facultad;
        this.fNacimiento = fNacimiento;
        this.direccion = direccion;
        this.latitud=latitud;
        this.longitud=longitud;
        this.imagen=imagen;
        this.extension=extension;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public String getfNacimiento() {
        return fNacimiento;
    }

    public void setfNacimiento(String fNacimiento) {
        this.fNacimiento = fNacimiento;
    }

    public String getDireccion() {  return direccion;   }

    public void setDireccion(String direccion) { this.direccion = direccion; }

    public String getLatitud() {    return latitud;  }

    public void setLatitud(String latitud) {     this.latitud = latitud; }

    public String getLongitud() {    return longitud;  }

    public void setLongitud(String longitud) {     this.longitud = longitud;  }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
