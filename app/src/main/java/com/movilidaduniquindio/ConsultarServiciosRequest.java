package com.movilidaduniquindio;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

/**
 * Esta es la clase de conexion para realizar la consulta de servicios al servidor web
 */
public class ConsultarServiciosRequest extends StringRequest {

    private static final String CONSULTAR_SERVICIOS_REQUEST_URL=Constantes.SERVIDOR+"/servicios/ConsultarServicios.php";
    private Map<String,String> params;

    /**
     * metodo encargado de hacer la peticion http al servidor web
     * @param listener este es parametro escuchador de la respuesta del servidor
     */
    public ConsultarServiciosRequest (Response.Listener<String> listener){

        super(Request.Method.POST,CONSULTAR_SERVICIOS_REQUEST_URL,listener,null);
    }

}
