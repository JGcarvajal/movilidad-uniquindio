package com.movilidaduniquindio;

import com.google.android.gms.maps.model.LatLng;

/**
 * Clase del objeto servicio
 */
public class Servicio {

    private String numDoc;
    private String idVehiculo;
    private LatLng latLngInicio;
    private LatLng latLngFin;
    private String fecha;
    private String hora;
    private String conductor;
    private int puestos;
    private String observacion;
    private String codConductor;

    /**
     * Constructor de la clase
     * @param numDoc identificador del servicio
     * @param latLngIni latitud/longitud de inicio del servicio
     * @param latLngFin latitud/longitud del servicio
     * @param fecha fecha del servicio
     * @param hora hora del servicio
     * @param conductor conductor que genera el servicio
     * @param puestos puestos disponibles en el servico
     * @param observacion observacion del servicio
     * @param idVehiculo identificacion del vehiculo que presta el servicio
     */
    public Servicio(String numDoc, LatLng latLngIni, LatLng latLngFin,String fecha, String hora,
                    String conductor, int puestos, String observacion, String idVehiculo,String codConductor) {
        this.numDoc = numDoc;
        this.idVehiculo=idVehiculo;
        this.latLngInicio=latLngIni;
        this.latLngFin=latLngFin;
        this.fecha = fecha;
        this.hora = hora;
        this.conductor = conductor;
        this.puestos = puestos;
        this.observacion = observacion;
        this.codConductor=codConductor;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public LatLng getLatLngInicio() {
        return latLngInicio;
    }

    public void setLatLngInicio(LatLng latLngInicio) {
        this.latLngInicio = latLngInicio;
    }

    public LatLng getLatLngFin() {
        return latLngFin;
    }

    public void setLatLngFin(LatLng latLngFin) {
        this.latLngFin = latLngFin;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public int getPuestos() {
        return puestos;
    }

    public void setPuestos(int puestos) {
        this.puestos = puestos;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(String idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getCodConductor() {
        return codConductor;
    }

    public void setCodConductor(String codConductor) {
        this.codConductor = codConductor;
    }
}
