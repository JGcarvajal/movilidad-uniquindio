package com.movilidaduniquindio;


import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Esta es la clase de conexion para realizar la peticion de registrar un usuario
 */
public class RegisterRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL=Constantes.SERVIDOR+"/servicios/RegisterPHP.php";
    private Usuario usuario;
    /**
     * metodo encargado de hacer la peticion http al servidor web
     * @param listener este es parametro escuchador de la respuesta del servidor
     */
    public RegisterRequest (Usuario usuario, Response.Listener<String> listener){
        super(Method.POST,REGISTER_REQUEST_URL,listener,null);
       this.usuario=usuario;

    }

    @Override
    public Map<String, String> getParams() {
        Map<String,String> params=new HashMap<>();

        String nombre =usuario.getNombres();
        String apellidos =usuario.getApellidos();
        String celular =usuario.getTelefono();
        String correo =usuario.getCorreo();
        String clave =usuario.getClave();
        String identificacion =usuario.getIdentificacion();
        String facultad =usuario.getFacultad();
        String fNacimiento =usuario.getfNacimiento();
        String direccion =usuario.getDireccion();
        String latitud =usuario.getLatitud();
        String longitud =usuario.getLongitud();
        String imagen =usuario.getImagen();
        String extension =usuario.getExtension();

        params.put("nombres",nombre);
        params.put("apellidos",apellidos);
        params.put("identificacion",identificacion);
        params.put("fNacimiento",fNacimiento);
        params.put("correo",correo);
        params.put("clave",clave);
        params.put("celular",celular);
        params.put("direccion",direccion);
        params.put("facultad",facultad);
        params.put("latitud",latitud);
        params.put("longitud",longitud);
        params.put("imagen",imagen);
        params.put("extension",extension);

        return params;
    }
}
